#include "Floor.h"

#include "FloorLayout.h"
#include "tile/VerticalWall.h"
#include "tile/HorizontalWall.h"
#include "tile/Passage.h"
#include "tile/Door.h"
#include "tile/FloorTile.h"
#include "tile/Nothing.h"
#include "../character/enemy/Human.h"
#include "../character/enemy/Dwarf.h"
#include "../character/enemy/Halfling.h"
#include "../character/enemy/Merchant.h"
#include "../character/enemy/Dragon.h"
#include "../character/enemy/Orc.h"
#include "../character/enemy/Elf.h"
#include "../item/potion/RestoreHealth.h"
#include "../item/potion/BoostAttack.h"
#include "../item/potion/BoostDefence.h"
#include "../item/potion/PoisonHealth.h"
#include "../item/potion/WoundAttack.h"
#include "../item/potion/WoundDefence.h"
#include "../item/treasure/NormalPile.h"
#include "../item/treasure/SmallPile.h"
#include "../item/treasure/MerchantHoard.h"
#include <sstream>
#include <typeinfo>

using namespace std;

void Floor::floodFill(Tile *t, Chamber *chamber) {
    if (t->getChamber() == chamber) return;

    string ty = typeid(t).name();
    FloorTile *tile = dynamic_cast<FloorTile *>(t);
    if (!tile) return;

    tile->setChamber(chamber);
    chamber->addTile(tile);

    Tile *next = tile->getNeighbor(N);
    if (next) floodFill(next, chamber);
    next = tile->getNeighbor(E);
    if (next) floodFill(next, chamber);
    next = tile->getNeighbor(S);
    if (next) floodFill(next, chamber);
    next = tile->getNeighbor(W);
    if (next) floodFill(next, chamber);

}

void Floor::deleteEnemiesAndItems() {
    for (auto iter = enemies.begin(); iter != enemies.end(); ++iter) {
        delete (*iter);
    }

    for (auto iter = items.begin(); iter != items.end(); ++iter) {
        delete (*iter);
    }
}

void Floor::resetFloor() {

    deleteEnemiesAndItems();

    for (auto iter1 = grid.begin(); iter1 != grid.end(); ++iter1) {
        for (auto iter2 = iter1->begin(); iter2 != iter1->end(); ++ iter2) {
            (*iter2)->reset();
        }
    }

}

Floor::Floor() {
    istringstream ss{FloorLayout::getLayout()};
    string line;

    int i = 0;
    int width = 0;
    while (true) {
        getline(ss, line);
        istringstream lineSs{line};
        if (ss.fail()) break;

        grid.push_back(vector<Tile *>());

        int j = 0;
        while (true) {
            lineSs >> noskipws;
            char c;

            lineSs >> c;
            if (lineSs.fail()) break;

            switch (c) {
                case '|':
                    grid[i].push_back(new VerticalWall(i, j));
                    break;
                case '-':
                    grid[i].push_back(new HorizontalWall(i, j));
                    break;
                case '#':
                    grid[i].push_back(new Passage(i, j));
                    break;
                case '+':
                    grid[i].push_back(new Door(i, j));
                    break;
                case '.':
                    grid[i].push_back(new FloorTile(i, j));
                    break;
                default:
                    grid[i].push_back(new Nothing(i, j));
                    break;
            }

            if (i != 0) {
                grid[i][j]->addNeighbor(grid[i-1][j], N);
                grid[i-1][j]->addNeighbor(grid[i][j], S);
                if (j != width) {
                    grid[i][j]->addNeighbor(grid[i-1][j+1], NE);
                    grid[i-1][j+1]->addNeighbor(grid[i][j], SW);
                }
            }
            if (j != 0) {
                grid[i][j]->addNeighbor(grid[i][j-1], W);
                grid[i][j-1]->addNeighbor(grid[i][j], E);
            }
            if (i != 0 && j != 0) {
                grid[i][j]->addNeighbor(grid[i-1][j-1], NW);
                grid[i-1][j-1]->addNeighbor(grid[i][j], SE);
            }

            ++j;
        }
        width = j-1;
        ++i;
    }

    floodFill(grid[5][9], chambers[0]);
    floodFill(grid[5][56], chambers[1]);
    floodFill(grid[12][47], chambers[2]);
    floodFill(grid[21][62], chambers[3]);
    floodFill(grid[20][18], chambers[4]);
}

Floor::~Floor() {
    for (auto iter1 = grid.begin(); iter1 != grid.end(); ++iter1) {
        for (auto iter2 = iter1->begin(); iter2 != iter1->end(); ++ iter2) {
            delete (*iter2);
        }
    }

    deleteEnemiesAndItems();

    for (Chamber *c: chambers) {
        delete c;
    }
}

void Floor::generate() {

    resetFloor();

    int playerSpawnChamber = rand() % 5;
    unsigned long tileIndex = rand() % chambers[playerSpawnChamber]->getFloor().size();
    playerSpawn = chambers[playerSpawnChamber]->getFloor().at(tileIndex);

    int stairsChamber = playerSpawnChamber;
    while (stairsChamber == playerSpawnChamber) {
        stairsChamber = rand() % 5;
    }
    tileIndex = rand() % chambers[stairsChamber]->getFloor().size();
    stairs = chambers[stairsChamber]->getFloor().at(tileIndex);
    stairs->makeStairs();

    int chamber;
    Tile *tile;

    for (int i = 0; i < 10; ++i) {
        do {
            chamber = rand() % 5;
            tileIndex = rand() % chambers[chamber]->getFloor().size();
            tile = chambers[chamber]->getFloor().at(tileIndex);
        } while (!tile->isEmpty() || tile == playerSpawn);
        items.push_back(tile->generateRandomPotion());
    }

    for (int i = 0; i < 10; ++i) {
        do {
            chamber = rand() % 5;
            tileIndex = rand() % chambers[chamber]->getFloor().size();
            tile = chambers[chamber]->getFloor().at(tileIndex);
        } while (!tile->isEmpty() || tile == playerSpawn);
        items.push_back(tile->generateRandomTreasure());
        DragonHoard *dh = dynamic_cast<DragonHoard *>(items.back());
        if (dh) {
            Tile *neighbor;
            do {
                tileIndex = rand() % 8;
                neighbor = tile->getNeighbor(static_cast<Direction>(tileIndex));
            } while (!neighbor);
            Dragon *d = new Dragon();
            d->setHoard(dh);
            dh->addDragonForHoard(d);
            enemies.push_back(neighbor->spawnEnemy(d));
        }
    }

    for (int i = 0; i < 20; ++i) {
        do {
            chamber = rand() % 5;
            tileIndex = rand() % chambers[chamber]->getFloor().size();
            tile = chambers[chamber]->getFloor().at(tileIndex);
        } while (!tile->isEmpty() || tile == playerSpawn);
        enemies.push_back(tile->spawnRandomEnemy());
    }

}

void Floor::update() {

    for (auto iter1 = grid.begin(); iter1 != grid.end(); ++iter1) {
        for (auto iter2 = iter1->begin(); iter2 != iter1->end(); ++iter2) {

            Tile *tile = *iter2;

            Enemy *e = nullptr;
            if (tile->getCharacter()) e = dynamic_cast<Enemy *>(tile->getCharacter());
            if (e) {
                e->update();
            }

        }
    }

}

void Floor::addObserversToPlayer(Player *p) {

    for (auto iter = enemies.begin(); iter != enemies.end(); ++iter) {
        p->addObserver(*iter);
    }

    for (auto iter = items.begin(); iter != items.end(); ++iter) {
        p->addObserver(*iter);
    }

}

Tile* Floor::getPlayerSpawn() {
    return playerSpawn;
}

Pos Floor::getStairsPos() {
    return stairs->getPos();
}

istream &operator>>(istream &in, Floor &floor) {

    floor.resetFloor();

    char c;
    string line;

    vector<Dragon *>dragons;
    Dragon *d;
    vector<DragonHoard *>dragonHoards;
    DragonHoard *dh;

    for (auto iter1 = floor.grid.begin(); iter1 != floor.grid.end(); ++iter1) {
        getline(in, line);
        istringstream ss{line};
        ss >> noskipws;
        for (auto iter2 = iter1->begin(); iter2 != iter1->end(); ++iter2) {
            Tile *tile = *iter2;
            ss >> c;
            switch (c) {
                case '@':
                    floor.playerSpawn = tile;
                    break;
                case '\\':
                    dynamic_cast<FloorTile *>(tile)->makeStairs();
                    floor.stairs = dynamic_cast<FloorTile *>(tile);
                    break;
                case 'H':
                    floor.enemies.push_back(tile->spawnEnemy(new Human()));
                    break;
                case 'W':
                    floor.enemies.push_back(tile->spawnEnemy(new Dwarf()));
                    break;
                case 'L':
                    floor.enemies.push_back(tile->spawnEnemy(new Halfling()));
                    break;
                case 'M':
                    floor.enemies.push_back(tile->spawnEnemy(new Merchant()));
                    break;
                case 'D':
                    d = new Dragon();
                    dragons.push_back(d);
                    for (auto iter = dragonHoards.begin(); iter != dragonHoards.end(); ++iter) {
                        if ((*iter)->getPos().distanceTo(tile->getPos()) < 2) {
                            (*iter)->addDragonForHoard(d);
                            d->setHoard(*iter);
                            dragonHoards.erase(iter);
                            dragons.pop_back();
                            break;
                        }
                    }
                    floor.enemies.push_back(tile->spawnEnemy(d));
                    break;
                case 'O':
                    floor.enemies.push_back(tile->spawnEnemy(new Orc()));
                    break;
                case 'E':
                    floor.enemies.push_back(tile->spawnEnemy(new Elf()));
                    break;
                case '0':
                    floor.items.push_back(tile->addItem(new RestoreHealth()));
                    break;
                case '1':
                    floor.items.push_back(tile->addItem(new BoostAttack()));
                    break;
                case '2':
                    floor.items.push_back(tile->addItem(new BoostDefence()));
                    break;
                case '3':
                    floor.items.push_back(tile->addItem(new PoisonHealth()));
                    break;
                case '4':
                    floor.items.push_back(tile->addItem(new WoundAttack()));
                    break;
                case '5':
                    floor.items.push_back(tile->addItem(new WoundDefence()));
                    break;
                case '6':
                    floor.items.push_back(tile->addItem(new SmallPile()));
                    break;
                case '7':
                    floor.items.push_back(tile->addItem(new NormalPile()));
                    break;
                case '8':
                    floor.items.push_back(tile->addItem(new MerchantHoard()));
                    break;
                case '9':
                    dh = new DragonHoard();
                    dragonHoards.push_back(dh);
                    for (auto iter = dragons.begin(); iter != dragons.end(); ++iter) {
                        if ((*iter)->getPos().distanceTo(tile->getPos()) < 2) {
                            (*iter)->setHoard(dh);
                            dh->addDragonForHoard(*iter);
                            dragons.erase(iter);
                            dragonHoards.pop_back();
                            break;
                        }
                    }
                    floor.items.push_back(tile->addItem(dh));
                    break;
                default:
                    break;
            }
        }
    }

    return in;

}

ostream &operator<<(ostream &out, Floor &floor) {

    for (auto iter1 = floor.grid.begin(); iter1 != floor.grid.end(); ++iter1) {
        for (auto iter2 = iter1->begin(); iter2 != iter1->end(); ++iter2) {
            out << *(*iter2);
        }
        out << endl;
    }
    return out;

}
