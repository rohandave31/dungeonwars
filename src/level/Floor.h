#ifndef Floor_H
#define Floor_H

#include <vector>
#include "tile/Tile.h"
#include "Chamber.h"
#include "../character/enemy/Enemy.h"

class Floor {

    std::vector<std::vector<Tile *>> grid;
    Chamber *chambers[5] = {new Chamber(), new Chamber(), new Chamber(), new Chamber(), new Chamber()};

    Tile *playerSpawn;
    FloorTile *stairs;
    std::vector<Enemy *> enemies;
    std::vector<Item *> items;

    //Use flood fill to determine the chambers
    void floodFill(Tile *t, Chamber *chamber);
    //Clear enemies and items
    void deleteEnemiesAndItems();
    //Reset the floor for next use
    void resetFloor();

public:

    Floor();
    ~Floor();

    //Generate items and enemies
    void generate();
    //Update enemies
    void update();
    //Add items and enemies as observers to the given Player
    void addObserversToPlayer(Player *p);

    Tile *getPlayerSpawn();
    Pos getStairsPos();

    friend std::istream &operator>>(std::istream &in, Floor &floor);
    friend std::ostream &operator<<(std::ostream &out, Floor &floor);

};


#endif