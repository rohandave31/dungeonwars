#ifndef Chamber_H
#define Chamber_H

#include "tile/FloorTile.h"

class Chamber {

    std::vector<FloorTile *> floor{};

public:

    void addTile(FloorTile *tile);

    std::vector<FloorTile *> getFloor();

};


#endif
