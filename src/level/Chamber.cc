#include "Chamber.h"
#include <typeinfo>

void Chamber::addTile(FloorTile *tile) {
    floor.push_back(tile);
}

std::vector<FloorTile *> Chamber::getFloor() {
    return floor;
}
