#ifndef Nothing_H
#define Nothing_H


#include "Tile.h"

class Nothing: public Tile {

public:

    Nothing(int x, int y);

    bool isTraversable() override;

};


#endif
