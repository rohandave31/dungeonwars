#ifndef Tile_H
#define Tile_H

#include <iostream>
#include <vector>
#include "../../item/Item.h"
#include "../../util/Pos.h"
#include "../../util/Direction.h"
#include "../../character/enemy/Enemy.h"
#include "../../character/player/Player.h"

class Character;
class Player;

class Tile {

    int x, y;
    Chamber *chamber;

protected:

    char display;
    Character *character = nullptr;
    Item *contents = nullptr;

    Tile *neighbors[8] = {nullptr};

    Tile(char display, int x, int y);



public:

    virtual ~Tile() = default;

    virtual bool isTraversable();
    virtual bool isEmpty();

    Character *getCharacter();
    Item *getContents();
    Chamber *getChamber();
    void setChamber(Chamber *chamber);

    //Spawn and add enemies and items to the tile
    Enemy *spawnRandomEnemy();
    Potion *generateRandomPotion();
    Treasure *generateRandomTreasure();
    Enemy *spawnEnemy(Enemy *e);
    Item *addItem(Item *item);
    void addPlayer(Player *p);

    //Move the character to a neighboring tile
    bool moveCharacter(Direction dir);

    //Clear character and contents fields
    void killCharacter();
    void useContents();

    Pos getPos();

    void addNeighbor(Tile *tile, Direction dir);
    Tile *getNeighbor(Direction dir);
    
    Direction getRandomDirection();
    Direction getDirectionToTile(Tile *tile);

    virtual void reset();

    friend std::ostream &operator<<(std::ostream &out, Tile &tile);

};


#endif
