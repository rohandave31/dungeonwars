#ifndef FloorTile_H
#define FloorTile_H


#include "Tile.h"

class FloorTile: public Tile {

    bool stairs = false;

public:

    FloorTile(int x, int y);

    void makeStairs();

    bool isEmpty() override;

    void reset() override;

};


#endif
