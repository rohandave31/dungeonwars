#include "Nothing.h"

Nothing::Nothing(int x, int y) : Tile{' ', x, y} {}

bool Nothing::isTraversable() {
    return false;
}
