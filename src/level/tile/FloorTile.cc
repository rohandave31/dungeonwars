#include "FloorTile.h"

FloorTile::FloorTile(int x, int y) : Tile{'.', x, y} {}

void FloorTile::makeStairs() {
    stairs = true;
    display = '\\';
}

bool FloorTile::isEmpty() {
    return (!contents && !character && !stairs);
}

void FloorTile::reset() {
    Tile::reset();
    stairs = false;
    display = '.';
}
