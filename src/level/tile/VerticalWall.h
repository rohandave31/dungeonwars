#ifndef VerticalWall_H
#define VerticalWall_H


#include "Wall.h"

class VerticalWall: public Wall {

public:

    VerticalWall(int x, int y);

};


#endif
