#include "Tile.h"
#include "../../util/Direction.h"
#include "../../character/player/Player.h"
#include "../../item/potion/Potion.h"
#include "../../item/treasure/Treasure.h"
#include "FloorTile.h"
#include "../../character/enemy/Human.h"
#include "../../character/enemy/Dwarf.h"
#include "../../character/enemy/Halfling.h"
#include "../../character/enemy/Elf.h"
#include "../../character/enemy/Orc.h"
#include "../../character/enemy/Merchant.h"
#include "../../item/potion/RestoreHealth.h"
#include "../../item/potion/PoisonHealth.h"
#include "../../item/potion/BoostAttack.h"
#include "../../item/potion/WoundAttack.h"
#include "../../item/potion/BoostDefence.h"
#include "../../item/potion/WoundDefence.h"
#include "../../item/treasure/NormalPile.h"
#include "../../item/treasure/SmallPile.h"
#include "../../item/treasure/DragonHoard.h"
#include <cstdlib>

using namespace std;

Tile::Tile(char display, int x, int y) : x{x}, y{y}, display{display} {}

bool Tile::isTraversable() {
    return true;
}

bool Tile::isEmpty() {
    return false;
}

Character *Tile::getCharacter() {
    return character;
}

Item *Tile::getContents() {
    return contents;
}

Chamber* Tile::getChamber() {
    return chamber;
}

void Tile::setChamber(Chamber *chamber) {
    this->chamber = chamber;
}

Enemy* Tile::spawnRandomEnemy() {

    Enemy *e;

    int x = rand() % 18;

    if (x < 4) {
        e = new Human();
    } else if (x < 7) {
        e = new Dwarf();
    } else if (x < 12) {
        e = new Halfling();
    } else if (x < 14) {
        e = new Elf();
    } else if (x < 16) {
        e = new Orc();
    } else {
        e = new Merchant();
    }

    character = e;
    e->setTile(this);

    return e;

}

Potion* Tile::generateRandomPotion() {

    Potion *p;

    int x = rand() % 60;
    if (x < 10) {
        p = new RestoreHealth();
    } else if (x < 20) {
        p = new PoisonHealth();
    } else if (x < 30) {
        p = new BoostAttack();
    } else if (x < 40) {
        p = new WoundAttack();
    } else if (x < 50) {
        p = new BoostDefence();
    } else {
        p = new WoundDefence();
    }

    contents = p;
    p->setTile(this);

    return p;

}

Treasure* Tile::generateRandomTreasure() {

    Treasure *t;

    int x = rand() % 8;

    if (x < 5) {
        t = new NormalPile();
    } else if (x < 7) {
        t = new SmallPile();
    } else {
        t = new DragonHoard();
    }

    contents = t;
    t->setTile(this);

    return t;

}

Enemy *Tile::spawnEnemy(Enemy *e) {
    character = e;
    e->setTile(this);
    return e;
}

Item *Tile::addItem(Item *item) {
    contents = item;
    item->setTile(this);
    return item;
}

void Tile::addPlayer(Player *p) {
    character = p;
}

bool Tile::moveCharacter(Direction dir) {

    Tile *target = neighbors[dir];
    if (!target || !target->isTraversable() || target->character) return false;

    Player *p = dynamic_cast<Player *>(character);
    FloorTile *tile = dynamic_cast<FloorTile *>(target);

    if (!target->contents && ((!p && tile && tile->isEmpty()) || p)) {
        target->character = character;
        character->setTile(target);
        character = nullptr;
        return true;
    }

    Treasure *t = dynamic_cast<Treasure *>(target->contents);

    if (p && t) {
        target->character = character;
        character->setTile(target);
        character = nullptr;
        return true;
    }

    return false;

}

void Tile::killCharacter() {
    character->setTile(nullptr);
    character = nullptr;
}

void Tile::useContents() {
    contents->setTile(nullptr);
    contents = nullptr;
}

void Tile::addNeighbor(Tile *tile, Direction dir) {
    neighbors[dir] = tile;
}

Tile* Tile::getNeighbor(Direction dir) {
    return neighbors[dir];
}

Direction Tile::getRandomDirection() {
    
    Tile *target = nullptr;
    Direction randomDir = N;
    
    while (true) {
        // Randomly choose a number from 0 to 7.
        randomDir = static_cast<Direction>(rand() % 8);
        target = neighbors[randomDir];
        
        if (target != nullptr && target->isTraversable()) {
            break;
        }
    }
    
    return randomDir;
}

Direction Tile::getDirectionToTile(Tile *other) {
    
    Pos otherPos = other->getPos();
    
    for (int i = 0; i < 8; i++) {
        if (neighbors[i]) {
            if (neighbors[i]->getPos() == otherPos) {
                return static_cast<Direction>(i);
            }
        }
    }
    
    return InvalidDir;
}

Pos Tile::getPos() {
    return {x, y};
}

void Tile::reset() {
    character = nullptr;
    contents = nullptr;
}

ostream &operator<<(ostream &out, Tile &tile) {
    if (tile.character) {
        out << *(tile.character);
    } else if (tile.contents) {
        out << *(tile.contents);
    } else {
        out << tile.display;
    }
    return out;
}
