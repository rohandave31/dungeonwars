#ifndef HorizontalWall_H
#define HorizontalWall_H


#include "Wall.h"

class HorizontalWall: public Wall {

public:

    HorizontalWall(int x, int y);

};


#endif
