#ifndef Wall_H
#define Wall_H


#include "Tile.h"

class Wall: public Tile {

protected:

    Wall(char display, int x, int y);

public:

    bool isTraversable() override;

};


#endif
