#ifndef Observer_H
#define Observer_H

#include "SubscriberType.h"

class Subject;


class Observer {
    
protected:
    SubscriberType subType;
    
public:
    Observer(SubscriberType type);
    virtual void getNotified(Subject *subject) = 0;
    SubscriberType getSubType();
};


#endif
