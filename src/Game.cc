#include "Game.h"
#include "character/player/Shade.h"
#include "character/player/Drow.h"
#include "character/player/Goblin.h"
#include "character/player/Vampire.h"
#include "character/player/Troll.h"
#include "util/Pos.h"
#include "ActionHandler.h"

using namespace std;

Direction getDirection(string dir) {
    if (dir == "nw") return NW;
    else if (dir == "no") return N;
    else if (dir == "ne") return NE;
    else if (dir == "ea") return E;
    else if (dir == "se") return SE;
    else if (dir == "so") return S;
    else if (dir == "sw") return SW;
    else if (dir == "we") return W;
    else return InvalidDir;
}

void Game::init(std::string race) {
    if (race == "s") {
        player = new Shade();
    } else if (race == "d") {
        player = new Drow();
    } else if (race == "v") {
        player = new Vampire();
    } else if (race == "g") {
        player = new Goblin();
    } else if (race == "t") {
        player = new Troll();
    }
    
    generateNextLevel();
}

void Game::generateNextLevel() {
    if (useFile) {
        in >> floor;
    } else {
        floor.generate();
    }
    player->clearObservers();
    floor.addObserversToPlayer(player);
    player->resetStatsForFloor();
    player->setTile(floor.getPlayerSpawn());
    floor.getPlayerSpawn()->addPlayer(player);
}

Game::Game(std::string race) : in{cin} {
    useFile = false;
    init(race);
}

Game::Game(std::string race, std::istream &in) : in{in} {
    useFile = true;
    init(race);
}

Game::~Game() {
    delete player;
}

void Game::playerMove(std::string dir) {

    if (player->move(getDirection(dir))) {
        ActionHandler::add("You move to the " + dir + ".");
    } else {
        ActionHandler::add("You try to move to the " + dir + " but something is in the way.");
    }

    if (player->getPos() == floor.getStairsPos()) {
        ++curFloor;
        if (curFloor < 6) {
            generateNextLevel();
        }
    } else {
        player->notifyObservers();
        if (runUpdates) floor.update();
    }

}

void Game::playerUse(std::string dir) {

    player->usePotion(getDirection(dir));

    player->notifyObservers();
    if (runUpdates) floor.update();

}

void Game::playerAttack(std::string dir) {

    player->attackk(getDirection(dir));

    player->notifyObservers();
    if (runUpdates) floor.update();

}

void Game::toggleEnemyUpdates() {
    runUpdates = !runUpdates;
}

bool Game::isWon() {
    return curFloor == 6;
}

bool Game::isLost() {
    return player->isDead();
}

int Game::getScore() {
    return player->getScore();
}

ostream &operator<<(ostream &out, Game &game) {
    out << game.floor;
    out << "Race: " << game.player->getName() << "\t\tGold: " << game.player->getGold();
    out << "\t\t\t" << "Floor: " << game.curFloor << endl;
    out << "HP: " << game.player->getHealthPoints() << endl;
    out << "Atk: " << game.player->getAttackPoints() << endl;
    out << "Def: " << game.player->getDefensePoints() << endl;
    out << "Action:" << ActionHandler::retrieve();
    return out;
}
