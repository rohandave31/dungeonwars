//
//  SubscriberType.h
//  src
//
//  Created by ROHAN DAVE on 2017-07-23.
//  Copyright © 2017 Rohan Dave. All rights reserved.
//

#ifndef SubscriberType_h
#define SubscriberType_h


enum SubscriberType {
    EnemySub,
    PotionSub
};


#endif /* SubscriberType_h */
