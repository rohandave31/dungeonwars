#ifndef ActionHandler_H
#define ActionHandler_H

#include <string>

class ActionHandler {

    static std::string actions;

public:

    //Add an action info string
    static void add(std::string action);

    //Retrieve all action strings since last retrieve
    static std::string retrieve();

};


#endif
