#include "Game.h"
#include <iostream>
#include <fstream>

using namespace std;

bool isDirection(string d) {
    return (d == "nw" || d == "no" || d == "ne" ||
            d == "ea" || d == "se" || d == "so" ||
            d == "sw" || d == "we");
}

int main(int argc, char *argv[]) {

    cin.exceptions(ios::failbit|ios::eofbit|ios::badbit);

    ifstream file;
    if (argc == 2) { 
        file = ifstream{argv[1]};
    }

    try {
        while (true) {

            cout << "Select a race (s, d, g, v, t): ";

            string race;
            cin >> race;

            if (race == "q") return 0;
            if (race != "s" && race != "d" && race != "g" &&
                    race != "v" && race != "t") {
                cout << "Invalid race!" << endl;
                continue;
            }
            Game *g;
            if (argc == 2) {
                g = new Game{race, file};
            } else {
                g = new Game{race};
            }

            cout << *g;

            string command;
            while (true) {

                cout << "Please enter a command: ";
                cin >> command;

                if (command == "q") {
                    delete g;
                    return 0;
                } else if (command == "r") break;
                else if (command == "f") g->toggleEnemyUpdates();
                else if (command == "u") {
                    string dir;
                    cin >> dir;
                    if (!isDirection(dir)) {
                        cout << "Invalid Direction!" << endl;
                        continue;
                    }
                    g->playerUse(dir);
                } else if (command == "a") {
                    string dir;
                    cin >> dir;
                    if (!isDirection(dir)) {
                        cout << "Invalid Direction!" << endl;
                        continue;
                    }
                    g->playerAttack(dir);
                } else if (isDirection(command)) {
                    g->playerMove(command);
                } else {
                    cout << "Invalid command!" << endl;
                    continue;
                }

                cout << *g;

                if (g->isWon()) {
                    cout << "You win! You got a score of " << g->getScore() << "." << endl;
                    break;
                } else if (g->isLost()) {
                    cout << "You lost!" << endl;
                    break;
                }

            }

            if (g->isWon() || g->isLost()) {

                cout << "Would you like to play again? (y/n):" << endl;

                string ans;
                cin >> ans;

                if (ans == "n") break;
                else if (ans != "y") return 1;

            }

            delete g;

        }
    } catch (ios::failure) {}

}
