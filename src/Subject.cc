#include <iostream>
#include "Subject.h"
#include "Observer.h"
#include "character/Character.h"
#include "item/Item.h"

using namespace std;

void Subject::addObserver(Observer *observer) {
    
    observers.push_back(observer);
}

void Subject::clearObservers() {
    
    observers.clear();
}

