#include "Observer.h"

Observer::Observer(SubscriberType type) : subType{type} {}

SubscriberType Observer::getSubType() {
    return subType;
}
