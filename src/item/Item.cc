#include "Item.h"
#include "../level/tile/Tile.h"

using namespace std;

Item::~Item() {}

Item::Item(char display, string name) : Observer{PotionSub}, display{display}, name{name} {}

void Item::setTile(Tile *t) {
    tile = t;
}

Tile* Item::getTile() {
    return tile;
}

Pos Item::getPos() {
    return tile->getPos();
}

std::string Item::getName() {
    return name;
}

ostream &operator<<(ostream &out, Item &item) {
    out << item.display;
    return out;
}
