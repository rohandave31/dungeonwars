#ifndef DragonHoard_H
#define DragonHoard_H

#include "Treasure.h"

class Dragon;


class DragonHoard: public Treasure {

private:
    Dragon *dragon;
    
public:
    DragonHoard();
    void addDragonForHoard(Dragon *dragon);
    bool canPickupTreasure() override;
};


#endif
