#ifndef Treasure_H
#define Treasure_H


#include "../Item.h"


class Treasure: public Item {

protected:
    int value;
    
public:
    Treasure(std::string name, int value);
    int pickup();
    virtual bool canPickupTreasure();
    void getNotified(Subject *subject) override; 
};


#endif
