#include "Treasure.h"
#include "../../Subject.h"
#include "../../character/player/Player.h"
#include "../../ActionHandler.h"

using namespace std;

Treasure::Treasure(string name, int value) : Item{'G', name}, value{value} {}

bool Treasure::canPickupTreasure() {
    return true;
}

int Treasure::pickup() {
    return value;
}

void Treasure::getNotified(Subject *subject) {

    Player *player = dynamic_cast<Player *>(subject);

    if (!player) return;

    if (player->getPos() == tile->getPos() && this->canPickupTreasure()) {
        player->pickupTreasure(*this);
        tile->useContents();
        ActionHandler::add("You pickup a " + getName() + " of gold.");
    }

}
