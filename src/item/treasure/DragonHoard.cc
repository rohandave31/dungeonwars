#include "DragonHoard.h"
#include "../../character/enemy/Dragon.h"


DragonHoard::DragonHoard() : Treasure{"Dragon Hoard", 6} {}

void DragonHoard::addDragonForHoard(Dragon *dragon) {
    this->dragon = dragon;
}

bool DragonHoard::canPickupTreasure() {
    return dragon->isDead();
}
