#ifndef BoostAttack_H
#define BoostAttack_H


#include "Potion.h"

class BoostAttack: public Potion {

private:
    static bool isUsed;
    
public:
    BoostAttack();
    void usePotion() override;
    bool isPotionUsed() override;
};


#endif
