#ifndef WoundAttack_H
#define WoundAttack_H


#include "Potion.h"

class WoundAttack: public Potion {

private:
    static bool isUsed;
    
public:
    WoundAttack();
    void usePotion() override;
    bool isPotionUsed() override;
};


#endif
