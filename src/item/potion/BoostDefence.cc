#include "BoostDefence.h"

bool BoostDefence::isUsed = false;

BoostDefence::BoostDefence() : Potion("BD", 0, 0, 5) {}

void BoostDefence::usePotion() {
    isUsed = true;
    Potion::usePotion();
}

bool BoostDefence::isPotionUsed() {
    return isUsed;
}
