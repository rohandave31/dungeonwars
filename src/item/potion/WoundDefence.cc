#include "WoundDefence.h"

bool WoundDefence::isUsed = false;

WoundDefence::WoundDefence() : Potion("WD", 0, 0, -5) {}

void WoundDefence::usePotion() {
    isUsed = true;
    Potion::usePotion();
}

bool WoundDefence::isPotionUsed() {
    return isUsed;
}
