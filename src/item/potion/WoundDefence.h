#ifndef WoundDefence_H
#define WoundDefence_H


#include "Potion.h"

class WoundDefence: public Potion {

private:
    static bool isUsed;
    
public:
    WoundDefence();
    void usePotion() override;
    bool isPotionUsed() override;
};


#endif
