#include "WoundAttack.h"

bool WoundAttack::isUsed = false;

WoundAttack::WoundAttack() : Potion("WA", 0, -5, 0) {}

void WoundAttack::usePotion() {
    isUsed = true;
    Potion::usePotion();
}

bool WoundAttack::isPotionUsed() {
    return isUsed;
}
