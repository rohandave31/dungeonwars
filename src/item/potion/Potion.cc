#include "Potion.h"
#include "../../character/player/Player.h"
#include "../../ActionHandler.h"

using namespace std;

Potion::Potion(string name, int healthEffect, int attackEffect, int defenseEffect) :
Item{'P', name},
healthEffect{healthEffect},
attackEffect{attackEffect},
defenseEffect{defenseEffect} {}

void Potion::usePotion() {
    tile->useContents();
}

int Potion::getHealthEffect() {
    return healthEffect;
}

int Potion::getAttackEffect() {
    return attackEffect;
}

int Potion::getDefenseEffect() {
    return defenseEffect;
}

void Potion::getNotified(Subject *subject) {

    if (!tile) return;
    
    Player *player = dynamic_cast<Player *>(subject);
    
    if (!player) return;

    if (player->getPos().distanceTo(tile->getPos()) < 2) {
        if (isPotionUsed()) {
            ActionHandler::add("You see a " + getName() + " potion.");
        } else {
            ActionHandler::add("You see an unknown potion.");
        }
    }
}
