#ifndef BoostDefence_H
#define BoostDefence_H


#include "Potion.h"

class BoostDefence: public Potion {

private:
    static bool isUsed;
    
public:
    BoostDefence();
    void usePotion() override;
    bool isPotionUsed() override;
};


#endif
