#include "RestoreHealth.h"

bool RestoreHealth::isUsed = false;

RestoreHealth::RestoreHealth() : Potion("RH", 10, 0, 0) {}

void RestoreHealth::usePotion() {
    isUsed = true;
    Potion::usePotion();
}

bool RestoreHealth::isPotionUsed() {
    return isUsed;
}
