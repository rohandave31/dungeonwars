#include "BoostAttack.h"

bool BoostAttack::isUsed = false;

BoostAttack::BoostAttack() : Potion("BA", 0, 5, 0) {}

void BoostAttack::usePotion() {
    isUsed = true;
    Potion::usePotion();
}

bool BoostAttack::isPotionUsed() {
    return isUsed;
}
