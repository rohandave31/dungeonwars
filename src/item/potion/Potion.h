#ifndef Potion_H
#define Potion_H

#include "../Item.h"

class Subject;


class Potion: public Item {

protected:

    Potion(std::string name, int healthEffect, int attackEffect, int defenseEffect);
    
    int healthEffect = 0;
    int attackEffect = 0;
    int defenseEffect = 0;
    
public:
    virtual void usePotion();
    virtual bool isPotionUsed() = 0;
    int getHealthEffect();
    int getAttackEffect();
    int getDefenseEffect();
    void getNotified(Subject *subject) override; 
};


#endif
