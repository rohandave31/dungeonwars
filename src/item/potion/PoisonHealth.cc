#include "PoisonHealth.h"

bool PoisonHealth::isUsed = false;

PoisonHealth::PoisonHealth() : Potion("PH", -10, 0, 0) {}

void PoisonHealth::usePotion() {
    isUsed = true;
    Potion::usePotion();
}

bool PoisonHealth::isPotionUsed() {
    return isUsed;
}
