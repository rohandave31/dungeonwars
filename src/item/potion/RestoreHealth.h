#ifndef RestoreHealth_H
#define RestoreHealth_H


#include "Potion.h"

class RestoreHealth: public Potion {

private:
    static bool isUsed;
    
public:
    RestoreHealth();
    void usePotion() override;
    bool isPotionUsed() override;
};


#endif

// TODO:
// use and attack() switch to direction
// return string on use and attack()
