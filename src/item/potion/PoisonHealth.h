#ifndef PoisonHealth_H
#define PoisonHealth_H


#include "Potion.h"

class PoisonHealth: public Potion {

private:
    static bool isUsed;
    
public:
    PoisonHealth();
    void usePotion() override;
    bool isPotionUsed() override;
};


#endif
