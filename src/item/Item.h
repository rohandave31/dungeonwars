#ifndef Item_H
#define Item_H

#include <iostream>
#include "../Observer.h"
#include "../util/Pos.h"

class Tile;

class Item: public Observer {

    char display;
    std::string name;

protected:

    Item(char display, std::string name);
    // The tile the item is currently placed on.
    Tile *tile;

public:

    virtual ~Item() = 0;

    void setTile(Tile *t);
    Tile *getTile();
    Pos getPos();

    std::string getName();

    friend std::ostream &operator<<(std::ostream &out, Item &item);

};


#endif
