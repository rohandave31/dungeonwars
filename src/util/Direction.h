#ifndef DIRECTION_H
#define DIRECTION_H

enum Direction {
    NW = 0,
    N = 1,
    NE = 2,
    E = 3,
    SE = 4,
    S = 5,
    SW = 6,
    W = 7,
    InvalidDir = 8
};

#endif
