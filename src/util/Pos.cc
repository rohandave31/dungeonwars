#include "Pos.h"
#include <math.h>

double Pos::distanceTo(const Pos &other) {
    return sqrt(pow(x-other.x, 2)+pow(y-other.y, 2));
}

bool Pos::operator==(const Pos &other) {
    return (x == other.x && y == other.y);
}
