#ifndef Pos_H
#define Pos_H

#include "Direction.h"

class Chamber;

struct Pos {
    int x, y;

    //Gets the distance between two points
    double distanceTo(const Pos &other);

    bool operator==(const Pos &other);
};

#endif
