#include "ActionHandler.h"

using namespace std;

string ActionHandler::actions = "";

void ActionHandler::add(std::string action) {
    actions += "\t- " + action + "\n";
}

std::string ActionHandler::retrieve() {
    string temp = actions;
    actions = "";
    return (temp == "" ? "\n" : temp);
}
