#ifndef Game_H
#define Game_H


#include "level/Floor.h"
#include "character/player/Player.h"

class Game {

    Floor floor;
    std::istream &in;

    Player *player;

    int curFloor = 0;
    bool runUpdates = true;
    bool useFile;

    //Initialize the player and generate the first floor
    void init(std::string race);
    //Move to the next floor and generate it
    void generateNextLevel();

public:

    Game(std::string race);
    Game(std::string race, std::istream &in);

    ~Game();

    //Methods to control the player
    void playerMove(std::string dir);
    void playerUse(std::string dir);
    void playerAttack(std::string dir);

    //Switch if enemies should update
    void toggleEnemyUpdates();

    //Game info
    bool isWon();
    bool isLost();
    int getScore();

    friend std::ostream &operator<<(std::ostream &out, Game &game);

};


#endif
