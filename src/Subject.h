#ifndef Subject_H
#define Subject_H

#include "SubscriberType.h"
#include <vector>

class Observer;


class Subject {
    
protected:
    std::vector<Observer *> observers;
    
public:
    void addObserver(Observer *observer);
    void clearObservers();
    virtual void notifyObservers() = 0;
};



#endif
