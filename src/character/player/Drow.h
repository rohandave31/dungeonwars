#ifndef Drow_H
#define Drow_H


#include "Player.h"

class Drow: public Player {

public:
    
    Drow();
    
    void usePotion(Direction direction) override;
};


#endif
