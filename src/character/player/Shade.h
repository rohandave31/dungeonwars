#ifndef Shade_H
#define Shade_H


#include "Player.h"

class Shade: public Player {

public:
    Shade();

    int getScore() override;
};


#endif
