#include "Troll.h"


Troll::Troll() : Player{"Troll", 120, 25, 15} {}

bool Troll::move(Direction direct) {
    
    // Also Increase 5 HP.
    healthPoints = (healthPoints <= maxHP - 5) ? healthPoints + 5 : maxHP;
    return false;
}

bool Troll::attackk(Direction dir) {

    Character *target = tile->getNeighbor(dir)->getCharacter();

    if (!target) return false;
    
    // Increase 5 HP on successful attack.
    if (target->getAttacked(this)) {
        healthPoints = (healthPoints <= maxHP - 5) ? healthPoints + 5 : maxHP;
    }
    
    return false;
}
