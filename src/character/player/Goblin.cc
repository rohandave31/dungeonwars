#include "Goblin.h"
#include "../enemy/Orc.h"
#include <cstdlib>
#include <math.h>

Goblin::Goblin() : Player{"Goblin", 110, 15, 20} {}

bool Goblin::getAttacked(Character *attacker) {
    
    // There is 50% chance that the enemies attack is missed.
    bool didMissAttack = (rand() % 100) <= 50;
    if (didMissAttack) return false;

    // Get attacked
    int damage = ceil((100 / (100 + this->defensePoints)) * attacker->getAttackPoints());
    
    if (dynamic_cast<Orc *>(attacker)) {
        // Orcs do 50% more damage to goblins.
        damage = ceil((damage / 2) + damage);
    }
    
    // Reduce HP by the damage.
    this->healthPoints = this->healthPoints - damage;
    if (this->healthPoints < 0) {
        this->healthPoints = 0;
    }
    
    return true;
}

void Goblin::killEnemy(Enemy *enemy) {
    
    // Steal 5 Gold from every slain enemy.
    gold += 5;
    
    // Also execute normal kill process.
    Player::killEnemy(enemy);
}
