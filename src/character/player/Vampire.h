#ifndef Vampire_H
#define Vampire_H


#include "Player.h"

class Vampire: public Player {
    
public:
    
    Vampire();
    
    bool attackk(Direction dir) override;
    void usePotion(Direction dir) override;
};


#endif
