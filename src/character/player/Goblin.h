#ifndef Goblin_H
#define Goblin_H


#include "Player.h"

class Goblin: public Player {

public:
    
    Goblin();
    
    bool getAttacked(Character *attacker) override;
    void killEnemy(Enemy *enemy) override;
};


#endif
