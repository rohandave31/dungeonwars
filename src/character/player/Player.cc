#include "Player.h"
#include "../enemy/Enemy.h"
#include "../enemy/Human.h"
#include "../enemy/Dragon.h"
#include "../../item/potion/Potion.h"
#include "../../item/treasure/Treasure.h"
#include "../../item/treasure/DragonHoard.h"
#include "../../ActionHandler.h"
#include <math.h>

using namespace std;

Player::Player(string name, int hp, int attck, int defns) :
Character{'@', name, hp, attck, defns} {}

bool Player::attackk(Direction dir) {

    Character *target = tile->getNeighbor(dir)->getCharacter();

    if (!target) {
        ActionHandler::add("You slash at the air in front of you.");
        return false;
    }
    
    if(target->getAttacked(this)) {
        // If attack was sucessfull, check if the enemies' HP is 0.
        // If it is 0, then kill the enemy.
        Enemy *enemy = dynamic_cast<Enemy *>(target);
        if (target->getHealthPoints() <= 0 && enemy) {
            ActionHandler::add("You kill a " + target->getName() + "!");
            killEnemy(enemy);
        }
        
        return true;
    } else {
        ActionHandler::add("You try to attack a " + target->getName() + " but miss!");
    }
    
    return false;
}

bool Player::getAttacked(Character *attacker) {
    
    // There is 50% chance that the enemies attack misses.
    bool didMissAttack = (rand() % 100) <= 50;
    if (didMissAttack) return false;
    
    // Get damage
    int damage = ceil((100.0 / (100 + this->defensePoints)) * attacker->getAttackPoints());
    // Reduce HP by the damage.
    healthPoints -= damage;
    ActionHandler::add("A " + attacker->getName() + " attacks you and deals " + to_string(damage) + " damage.");
    if (healthPoints <= 0) tile->killCharacter();
    
    return true;
}

bool Player::move(Direction direct) {

    return tile->moveCharacter(direct);
}

void Player::pickupTreasure(Treasure &treasure) {
    
    if(treasure.canPickupTreasure()) {
        gold += treasure.pickup();
    }
}

void Player::killEnemy(Enemy *enemy) {

    if (dynamic_cast<Dragon *>(enemy)) return;

    gold += (enemy->getDropped()).pickup();

    if (dynamic_cast<Human *>(enemy)) {
        // Humans always give two piles of Normal Gold.
        gold += enemy->getDropped().pickup();
    }

}

void Player::usePotion(Direction dir) {

    Item *item = tile->getNeighbor(dir)->getContents();

    if (!item) return;

    Potion *potion = dynamic_cast<Potion *>(item);

    if (!potion) return;
    
    int newHP = healthPoints + potion->getHealthEffect();
    healthPoints = (newHP > maxHP) ? maxHP : newHP;
    
    int newAttack = attackPoints + potion->getAttackEffect();
    attackPoints = (newAttack < 0) ? 0 : newAttack;
    
    int newDefns = defensePoints + potion->getDefenseEffect();
    defensePoints = (newDefns < 0) ? 0 : newDefns;
    
    potion->usePotion();

    ActionHandler::add("You use a " + potion->getName() + " potion.");
}

void Player::resetStatsForFloor() {
    
    // Don't change the HP.
    attackPoints = maxAttck;
    defensePoints = maxDefns;
}

void Player::checkStatsLimits() {
    
    if (attackPoints < 0) {
        attackPoints = 0;
    }
    
    if (defensePoints < 0) {
        defensePoints = 0;
    }
}

void Player::notifyObservers() {

    for (auto observer = observers.begin(); observer != observers.end(); ++observer) {
        if ((*observer)->getSubType() == SubscriberType::EnemySub) {
            (*observer)->getNotified(this);
        } else {
            Tile *t;

            Character *c = dynamic_cast<Character *>(*observer);
            if (c) {
                t = c->getTile();
            } else {
                Item *it = dynamic_cast<Item *>(*observer);
                t = it->getTile();
            }
            if (t && t->getChamber() == this->getTile()->getChamber()) {
                (*observer)->getNotified(this);
            }
        }
    }
}

int Player::getGold() {
    return gold;
}

int Player::getScore() {
    return gold;
}
