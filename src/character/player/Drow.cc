#include "Drow.h"
#include "../../item/potion/Potion.h"
#include <math.h>


Drow::Drow() : Player{"Drow", 150, 25, 15} {}

void Drow::usePotion(Direction dir) {

    Item *item = tile->getNeighbor(dir)->getContents();

    if (!item) return;

    Potion *potion = dynamic_cast<Potion *>(item);

    if (!potion) return;
    
    healthPoints += floor(potion->getHealthEffect() * 1.5);
    if (healthPoints > maxHP) {
        // HP is capped to the default max.
        healthPoints = maxHP;
    }
    
    attackPoints += floor(potion->getAttackEffect() * 1.5);
    defensePoints += floor(potion->getDefenseEffect() * 1.5);
    
    Player::checkStatsLimits();
    
    potion->usePotion();
}
