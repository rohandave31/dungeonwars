#include "Vampire.h"
#include "../../item/potion/Potion.h"
#include "../enemy/Dwarf.h"


Vampire::Vampire() : Player{"Vampire", 50, 25, 25} {}

bool Vampire::attackk(Direction dir) {

    Character *target = tile->getNeighbor(dir)->getCharacter();

    if (!target) return false;
    
    if (target->getAttacked(this)) {
        if (dynamic_cast<Dwarf *>(target)) {
            // If attacked a Dward then lose 5 HP.
            healthPoints -= 5;
        } else {
            healthPoints += 5;
        }
        return true;
    }
    
    return false;
}

void Vampire::usePotion(Direction dir) {

    Item *item = tile->getNeighbor(dir)->getContents();

    if (!item) return;

    Potion *potion = dynamic_cast<Potion *>(item);

    if (!potion) return;
    
    // They can have unlimited HP, no upper limit.
    healthPoints += potion->getHealthEffect();
    attackPoints += potion->getAttackEffect();
    defensePoints += potion->getDefenseEffect();
    
    Player::checkStatsLimits();
    
    potion->usePotion();
}
