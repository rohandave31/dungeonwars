#ifndef Player_H
#define Player_H


#include "../Character.h"
#include "../../Subject.h"
#include "../../level/tile/Tile.h"

class Player: public Character, public Subject {
 
protected:

    Player(std::string name, int hp, int attck, int defns);
    void checkStatsLimits();
    int gold = 0;

public:
    
    // Move the character by the directions passed.
    virtual bool move(Direction direct);
    // Kill the enemy passed as parameter.
    virtual void killEnemy(Enemy *enemy);
    // Use the potion.
    virtual void usePotion(Direction dir);
    // Pickup the passed treasure.
    virtual void pickupTreasure(Treasure &treasure);
    // Attack a enemy target passed as parameter
     virtual bool attackk(Direction dir) override;
    // Get attacked by the attacker passed as parameter
    virtual bool getAttacked(Character *attacker) override;
    // Resets the player stats for each new floor.
    virtual void resetStatsForFloor();
    void notifyObservers() override;

    int getGold();
    virtual int getScore();

};


#endif
