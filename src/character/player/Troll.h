#ifndef Troll_H
#define Troll_H


#include "Player.h"

class Troll: public Player {
    
public:
    
    Troll();
    
    bool move(Direction direct) override;
    bool attackk(Direction dir) override;
};


#endif
