#include "Character.h"
#include "../level/tile/Tile.h"

using namespace std;

Character::Character(char display, string name, int hp, int attck, int defns) :
display{display},
name{name},
healthPoints{hp},
attackPoints{attck},
defensePoints{defns},
maxHP{healthPoints},
maxAttck{attackPoints},
maxDefns{defensePoints} {}

int Character::getHealthPoints() {
    return healthPoints;
}

int Character::getAttackPoints() {
    return attackPoints;
}

int Character::getDefensePoints() {
    return defensePoints;
}

void Character::setTile(Tile *t) {
    tile = t;
}

Tile *Character::getTile() {
    return tile;
}

Pos Character::getPos() {
    return tile->getPos();
}

std::string Character::getName() {
    return name;
}

bool Character::isDead() {
    return healthPoints <= 0;
}

ostream &operator<<(ostream &out, Character &c) {
    out << c.display;
    return out;
}
