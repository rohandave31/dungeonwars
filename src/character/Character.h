#ifndef Character_H
#define Character_H

#include <iostream>
#include "../util/Direction.h"
#include "../util/Pos.h"

class Enemy;
class Potion;
class Treasure;
class Tile;


class Character {

    char display;
    std::string name;

protected:

    Character(char display, std::string name, int hp, int attck, int defns);
    
    int healthPoints;
    int attackPoints;
    int defensePoints;
    
    // The max / default values for character stats.
    int maxHP;
    int maxAttck;
    int maxDefns;
    
    Tile *tile;

public:

    virtual ~Character() = default;
    
    // Attack a player target passed as parameter
    virtual bool attackk(Direction dir) = 0;
    // Get attacked by the attacker passed as parameter
    virtual bool getAttacked(Character *attacker) = 0;
    
    // Getters
    int getHealthPoints();
    int getAttackPoints();
    int getDefensePoints();

    void setTile(Tile *t);
    Tile *getTile();
    Pos getPos();

    std::string getName();

    bool isDead();
    
    friend std::ostream &operator<<(std::ostream &out, Character &c);

};


#endif
