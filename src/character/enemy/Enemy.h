#ifndef Enemy_H
#define Enemy_H


#include "../Character.h"
#include "../../Observer.h"
#include "../../level/tile/Tile.h"

class Treasure;

class Enemy: public Character, public Observer {

    bool shouldAttack = false;
    bool shouldMove = false;
    Direction nextAttackDirection;
    
protected:
    Enemy(char display, std::string name, int hp, int attck, int defns);
    
public:
    
    virtual void update();
    virtual Treasure getDropped();
    // Attack a player target passed as parameter
    virtual bool attackk(Direction dir) override;
    // Get attacked by the attacker passed as parameter
    virtual bool getAttacked(Character *attacker) override;
    virtual void getNotified(Subject *subject) override;

};


#endif
