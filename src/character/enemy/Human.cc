#include "Human.h"
#include "../../item/treasure/NormalPile.h"


Human::Human() : Enemy{'H', "Human", 140, 20, 20} {}

Treasure Human::getDropped() {
    
    return NormalPile();
}
