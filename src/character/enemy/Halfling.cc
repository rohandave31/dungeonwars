#include "Halfling.h"

Halfling::Halfling() : Enemy{'L', "Halfling", 100, 15, 20} {}


bool Halfling::getAttacked(Character *attacker) {
    
    // There is 50% chance that the Players attack misses on Halfling.
    bool didMissAttack = (rand() % 100) <= 50;
    if (didMissAttack) return false;
    
    return Enemy::getAttacked(attacker);
}
