#ifndef Elf_H
#define Elf_H


#include "Enemy.h"

class Elf: public Enemy {

public:

    Elf();
    
    bool attackk(Direction dir) override;
};


#endif
