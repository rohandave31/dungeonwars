#ifndef Human_H
#define Human_H


#include "Enemy.h"


class Human: public Enemy {

public:

    Human();
    
    Treasure getDropped() override;
};


#endif
