#ifndef Dragon_H
#define Dragon_H


#include "Enemy.h"
#include "../../item/treasure/DragonHoard.h"

class Dragon: public Enemy {

    DragonHoard *hoard;

    Player *playerToAttack;
    bool shouldAttack;

    bool attack(Player *player);

public:

    Dragon();

    void update() override;
    
    void setHoard(DragonHoard *dh);
    
    DragonHoard *getHoard();

    void getNotified(Subject *who) override;
};


#endif
