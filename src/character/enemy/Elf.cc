#include "Elf.h"
#include "../player/Drow.h"


Elf::Elf() : Enemy{'E', "Elf", 140, 30, 10} {}

bool Elf::attackk(Direction dir) {

    Character *target = tile->getNeighbor(dir)->getCharacter();

    if (!target) return false;
    
    // First attack.
    bool first = target->getAttacked(this);
    bool second = false;
    
    if (dynamic_cast<Drow *>(target)) {
        // Gets second attack agains Drows.
        second = target->getAttacked(this);
    }
    
    return (first || second);
}
