#include "Enemy.h"
#include "../player/Player.h"
#include "../../item/treasure/SmallPile.h"
#include "../../item/treasure/NormalPile.h"
#include "../../ActionHandler.h"
#include <cstdlib>
#include <math.h>

using namespace std;

Enemy::Enemy(char display, string name, int hp, int attck, int defns) :
Character{display, name, hp, attck, defns}, Observer{EnemySub} {}

Treasure Enemy::getDropped() {
    
    // 50% chance of getting small or normal pile.
    if ((rand() % 100) <= 50) {
        return SmallPile();
    }
    
    return NormalPile();
}

void Enemy::update() {

    if (!tile) return;
    
    if (shouldMove) {
        this->tile->moveCharacter(this->tile->getRandomDirection());
        shouldMove = false;
    }
    
    if (shouldAttack && nextAttackDirection != InvalidDir) {
        this->attackk(nextAttackDirection);
        shouldAttack = false;
    }
}

bool Enemy::attackk(Direction dir) {

    Character *target = tile->getNeighbor(dir)->getCharacter();

    if (!target) return false;

    return target->getAttacked(this);
}

bool Enemy::getAttacked(Character *attacker) {
    
    // Players attack on enemy never misses.
    // Get damage
    int damage = ceil((100.0 / (100 + this->defensePoints)) * attacker->getAttackPoints());
    // Reduce HP by the damage.
    this->healthPoints = this->healthPoints - damage;
    
    if (this->healthPoints <= 0) {
        this->healthPoints = 0;
        tile->killCharacter();
    } else {
        ActionHandler::add("You deal " + to_string(damage) + " to a " + getName() + " (" + to_string(healthPoints) + " HP)");
    }
    
    return true;
}

void Enemy::getNotified(Subject *subject) {

    if (!tile) return;
    
    Player *player = dynamic_cast<Player *>(subject);
    
    if (!player) return;
    
    if (player->getPos().distanceTo(this->getPos()) < 2) {
        nextAttackDirection = this->tile->getDirectionToTile(player->getTile());
        shouldAttack = true;
    } else {
        shouldMove = true;
    }
}
