#include "Dragon.h"

Dragon::Dragon() : Enemy{'D', "Dragon", 150, 20, 20} {}

bool Dragon::attack(Player *player) {

    if (!player) return false;

    return player->getAttacked(this);

}

void Dragon::update() {

    if (!tile) return;

    if (shouldAttack && playerToAttack) {
        this->attack(playerToAttack);
        shouldAttack = false;
    }

}

void Dragon::setHoard(DragonHoard *dh) {
    hoard = dh;
}

DragonHoard *Dragon::getHoard() {
    return hoard;
}

void Dragon::getNotified(Subject *subject) {

    if (!tile) return;

    Player *player = dynamic_cast<Player *>(subject);

    if (!player) return;

    if (player->getPos().distanceTo(this->getPos()) < 2 ||
            player->getPos().distanceTo(hoard->getPos()) < 2) {
        playerToAttack = player;
        shouldAttack = true;
    }
}
