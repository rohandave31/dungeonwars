#ifndef Halfling_H
#define Halfling_H


#include "Enemy.h"

class Halfling: public Enemy {

public:

    Halfling();
    
    // Get attacked by the attacker passed as parameter
    bool getAttacked(Character *attacker) override;
};


#endif
