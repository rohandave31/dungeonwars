#include "Merchant.h"
#include "../../item/treasure/MerchantHoard.h"

bool Merchant::isHostile = false;

Merchant::Merchant() : Enemy{'M', "Merchant", 30, 70, 5} {
    
    isHostile = false;
}

bool Merchant::attackk(Direction dir) {
    
    // Can't attack if Merchant is not hostile.
    if (!isHostile) return false;
    
    return Enemy::attackk(dir);
}

bool Merchant::getAttacked(Character *attacker) {
    
    // Merchant is now hostile
    isHostile = true;
    
    return Enemy::getAttacked(attacker);
}

Treasure Merchant::getDropped() {
    return MerchantHoard();
}
