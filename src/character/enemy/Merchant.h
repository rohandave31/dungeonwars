#ifndef Merchant_H
#define Merchant_H


#include "Enemy.h"

class Merchant: public Enemy {

private:
    // Merchants are not hostile until the player attacks or slays one of them.
    static bool isHostile;
    
public:
    Merchant();
    bool attackk(Direction dir) override;
    bool getAttacked(Character *attacker) override;
    virtual Treasure getDropped() override;
};


#endif
